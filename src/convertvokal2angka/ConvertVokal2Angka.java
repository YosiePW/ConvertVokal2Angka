
package convertvokal2angka;
import java.util.Scanner;
public class ConvertVokal2Angka {

    private static String tampilInput;

    public static void tampilJudul(String identitas) {
        System.out.println("identitas : " + identitas);
        System.out.println("\nConvert kalimat alay angka (Vokal ke angka)\n");          
    }
    
    public static void main(String[] args) {
        String identitas = "Yosieka Putri Wibawa / X RPL 6 / 40";
        tampilJudul(identitas);
        String kalimat = tampilInput;
        String convert = vocal2Angka(kalimat);
        tampilPerKata(kalimat, convert);
        tampilHasil(convert);
    }
    public static String tampilInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("masukkan Kalimat : ");
        String kalimat = scanner.nextLine();
        System.out.println("kalimat asli : " + kalimat);
        return kalimat;
    }
    private static String vocal2Angka(String kalimat) {
        char [][] arConvert = {{'a', '4'},{'i', 'l'},{'u', '2'},{'e', '3'},{'o', '0'}};
                kalimat = kalimat.toLowerCase();
                for(int i=0; i<arConvert.length; i++)
                    kalimat = kalimat.replace(arConvert[i][0], arConvert[i][1]);
                return kalimat;    
    }
    private static void tampilPerKata(String kalimat, String convert) {
        String[] arrKal = kalimat.split(" ");
        String[] arrCon = convert.split(" ");
        for (int i=0; i<arrKal.length; i++)
            System.out.println(arrKal[i] + " => " + arrCon[i]);           
    }
    private static void tampilHasil(String convert) {
        System.out.println("Kalimat alay angka : " + convert);
    }
}
